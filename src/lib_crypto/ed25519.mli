(*****************************************************************************)
(*                                                                           *)
(* Open Source License                                                       *)
(* Copyright (c) 2018 Dynamic Ledger Solutions, Inc. <contact@tezos.com>     *)
(*                                                                           *)
(* Permission is hereby granted, free of charge, to any person obtaining a   *)
(* copy of this software and associated documentation files (the "Software"),*)
(* to deal in the Software without restriction, including without limitation *)
(* the rights to use, copy, modify, merge, publish, distribute, sublicense,  *)
(* and/or sell copies of the Software, and to permit persons to whom the     *)
(* Software is furnished to do so, subject to the following conditions:      *)
(*                                                                           *)
(* The above copyright notice and this permission notice shall be included   *)
(* in all copies or substantial portions of the Software.                    *)
(*                                                                           *)
(* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR*)
(* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,  *)
(* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL   *)
(* THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER*)
(* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING   *)
(* FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER       *)
(* DEALINGS IN THE SOFTWARE.                                                 *)
(*                                                                           *)
(*****************************************************************************)

(** Tezos - Ed25519 cryptography *)

open S

include sig

  module Public_key_hash : sig

    type t

    val pp: Format.formatter -> t -> unit
    val pp_short: Format.formatter -> t -> unit
    include Compare.S with type t := t
    include RAW_DATA with type t := t
    include B58_DATA with type t := t
    include ENCODER with type t := t
    include INDEXES with type t := t

    val zero: t

    module Logging : sig
      val tag : t Tag.def
    end
  end

  module Public_key : sig

    type t

    val pp: Format.formatter -> t -> unit
    val of_string_opt : string -> t option
    val to_string : t -> string
    include Compare.S with type t := t
    include B58_DATA with type t := t
    include ENCODER with type t := t

    val hash: t -> Public_key_hash.t
  end

  module Secret_key : sig

    type t

    val pp: Format.formatter -> t -> unit
    include Compare.S with type t := t
    include B58_DATA with type t := t
    include ENCODER with type t := t

    val to_public_key: t -> Public_key.t

  end

  type t

  val pp: Format.formatter -> t -> unit
  include Compare.S with type t := t
  include B58_DATA with type t := t
  include ENCODER with type t := t

  val zero: t

  type watermark
  val sign: ?watermark:watermark -> Secret_key.t -> MBytes.t -> t
  val check: ?watermark:watermark -> Public_key.t -> t -> MBytes.t -> bool

  val generate_key: ?seed:MBytes.t -> unit -> (Public_key_hash.t * Public_key.t * Secret_key.t)

end with type watermark = MBytes.t

include S.RAW_DATA with type t := t
