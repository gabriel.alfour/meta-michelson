open Meta_michelson_helpers.Michelson_wrap
open Meta_michelson_helpers.Michelson_modules
open Meta_michelson_helpers.Contract

module Full_contract = struct
  open Proto_alpha
  open Script_typed_ir

  module Storage : sig
    module A : Nat.NAT
    module B : Nat.NAT

    type t
    val ty : t ty

    val get_a : (t * 'r, A.t * 'r) code
    val get_b : (t * 'r, B.t * 'r) code
    val set_a : (A.t * (t * 'r), t * 'r) code
    val set_b : (B.t * (t * 'r), t * 'r) code
  end = struct
    module A = Nat.Make(val t_M)
    module B = Nat.Make(val t_M)

    type t = A.t * B.t
    let ty : t ty = Types.pair A.ty B.ty

    let get_a : (t * 'r, A.t * 'r) code = car
    let get_b : (t * 'r, B.t * 'r) code = cdr
    let set_a : (A.t * (t * 'r), t * 'r) code = fun s -> !: (s <. dip cdr <: pair)
    let set_b : (B.t * (t * 'r), t * 'r) code = fun s -> (dip car @| swap @| pair) s
  end

  module Free_money = struct
    include Nat.Make(val t_M)
  end

  module Cards_meta = struct
    module Card_id = Nat.Make(val t_M)
    module Rarity = Nat.Make(val t_M)
    let quantity_ty = Types.nat
    module Card_data = (val wrap (Types.pair Rarity.ty quantity_ty))
    module Card_table = Sized_map(Card_id)(Card_data)

    let increment_card_count : (Card_data.t * 'r, Card_data.t * 'r) code = fun bef ->
      !: (bef <. unpair <: swap <: push_nat 1 <: add_natnat <: swap <: pair)

    let compute_price : (Card_data.t * 'r, nat * 'r) code = fun bef ->
      !: (bef <. unpair <: Rarity.rename_from <: mul_natnat)

    let create_assert : (Card_id.t * (Card_table.t * (Free_money.t * 'r)), Card_table.t * (Free_money.t * 'r)) code = fun bef ->
      !: ( bef <.
           keep2 Card_table.get <: bubble_2 <:
           increment_card_count <:
           keep1 compute_price <: bubble_1 <:
           keep1 (amount_nat @| swap @| assert_cmp_ge_nat) <:
           dip (swap @| Card_table.set) <:
           bubble_2 <: swap <: push_nat 100 <: swap <: div <: dip Free_money.rename_from <: add_natnat <: Free_money.rename_to <:
           swap
         )
  end

end

