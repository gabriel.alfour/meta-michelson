(*
 * Copyright (C) 2019, Coase, inc
*)

open Meta_michelson_helpers.Michelson_wrap
open Meta_michelson_helpers.Michelson_modules
open Meta_michelson_helpers.Contract
open Proto_alpha.Script_typed_ir

module Value = struct
  type t = string
  let ty = Types.string
  let cmp = fun s -> !: (s <. cmp Types.string_k)
  let compare = compare
end

module Key (ID : sig val id : int end) = struct
  let current_id = ID.id

  type t = nat * nat (* ID *)
  let ty = Types.(pair nat nat)
  let raw_to_int n =
    Option.unopt_exn (Failure "Linked Heap : raw_size")
      (AC.Script_int.to_int (snd n))
  let raw_of_int n = Values.(nat current_id, nat n)

  let of_nat = fun s -> !: (s <. push_nat current_id <: pair)
  let to_nat = cdr
end

open Meta_michelson_helpers.Heap

module Proto_card : sig
  type t
  val ty : t ty

  module Rarity : WRAP with type t = nat
  val make : Rarity.t -> t
  val get_rarity : (t * 'r, Rarity.t * 'r) code
end = struct
  module Rarity = (val wrap Types.nat)
  include Rarity

  let make x = x
  let get_rarity = noop
end

module Proto_cards = struct
  module Id = (val wrap Types.nat)
  type t = (Id.t, Proto_card.t) Proto_alpha.Script_typed_ir.map
  let ty : t ty = Types.map Types.nat_k Proto_card.ty

  let raw_empty : t = Proto_alpha.Script_ir_translator.empty_map Types.nat_k
  let raw_of_list lst : t =
    let lst = List.mapi (fun x y -> Values.nat x, y) lst in
    let aux acc (i, m) =
      Proto_alpha.Script_ir_translator.map_update i (Some m) acc
    in
    List.fold_left aux raw_empty lst

  let get (type r) : (Id.t * (t * r), Proto_card.t * r) code =
      fun s -> !: (s <. map_get Id.ty Proto_card.ty <: force_some)

  let get_rarity (type r) : (Id.t * (t * r), Proto_card.Rarity.t * r) code =
    fun s -> !: (s <. get <: Proto_card.get_rarity)

end

module Bid = struct
  module Price = (val wrap Types.nat)
  module Quantity = (val wrap Types.nat)
  module Priority = (val wrap Types.nat)
  include Pair(Pair(Price)(Quantity))(Priority)

  let make (type r) : (Price.t * (Quantity.t * (Priority.t * r)), t * r) code = fun s -> !: (s <. pair <: pair)
  let raw_to_string : (t -> string) = fun ((a, b), c) ->
    Format.sprintf "P%d, Q%d, #%d" (Values.nat_to_int a) (Values.nat_to_int b) (Values.nat_to_int c)

  let get_price (type r) : (t * r, Price.t * r) code = fun s -> !: (s <. car <: car)
  let get_quantity (type r) : (t * r, Quantity.t * r) code = cdr
  let change_quantity (type r) : (Quantity.t * (t * r), t * r) code = fun s -> !: (
      s <.
      dip (unpair @| unpair @| dip drop) <:
      swap <: pair <: pair
    )
  let compare = ()
  let cmp (type r) : (t * (t * r), int_num * r) code = fun s -> !: (
      s <.
      keep2 (fun s -> !: (
          s <.
          car <: car <: dip (car @| car) <:
          cmp Types.nat_k
        )) <:
      bubble_2 <: dup <: eq <: if_bool (drop @| cdrcdr @| cmp Types.nat_k) (dip (drop @| drop))
    )
  let compare (x:t) (y:t) : int =
    let price_cmp = AC.Script_int.compare (fst @@ fst x) (fst @@ fst y) in
    if price_cmp = 0
    then AC.Script_int.compare (snd x) (snd y)
    else price_cmp

end

module Auction = struct
  module LocalStorage = struct
    module Start_time = (val wrap Types.timestamp)
    module End_time = (val wrap Types.timestamp)
    type timestamp = AC.Script_timestamp.t
    module Total_quantity = (val wrap Types.nat)
    module Heap = LinkedHeap(Key(struct let id = 0 end))(Bid)
    module Proto_card_id = Proto_cards.Id
    module Max_priority = (val wrap Types.nat)

    include Pair
        (Pair(Start_time)(End_time))
        (Pair
           (Pair(Proto_card_id)(Max_priority))
           (Pair(Total_quantity)(Heap)))

    let make
        (s:Start_time.t) (e:End_time.t)
        (id:Proto_card_id.t) : t =
      ((s, e), ((id, Values.nat 0), (Values.nat 0, Heap.empty)))
    
    let get_id (type r) : (t * r, Proto_card_id.t * r) code = fun s -> !: (s <. cdr <: car <: car)
    let get_max_priority (type r) : (t * r, nat * r) code = fun s -> !: (s <. cdr <: car <: cdr)
    let set_max_priority (type r) : (nat * (t * r), t * r) code = fun s -> !: (
        s <.
        dip (fun s -> !: (
            s <.
            unpair <:
            swap <: unpair <:
            unpair <: swap <:
            drop
          )) <:
        swap <: pair <:
        pair <:
        swap <:
        pair
      )
    let increase_max_priority (type r) : (t * r, nat * (t * r)) code = fun s -> !: (
        s <.
        save_1_1 get_max_priority <:
        add_n 1 <:
        keep1 set_max_priority
      )
    let get_total_quantity (type r) : (t * r, Total_quantity.t * r) code = fun s -> !: (s <. cdr <: cdr <: car)
    let get_heap (type r) : (t * r, Heap.t * r) code = fun s -> !: (s <. cdr <: cdr <: cdr)
    let set_heap (type r) : (Heap.t * (t * r), t * r) code = fun s -> !: (
        s <.
        swap <:
        dup <: car <:
        dip (dup @| cdr @| car) <:
        diip (cdr @| cdr @| car) <:
        diip pair <: dip pair <: pair
      )
  end

  type t = LocalStorage.t
  let ty = LocalStorage.ty
  
  type quantity = nat

  let insert_bid (type r) : (Bid.t * (t * r), t * r) code = fun s -> !: (
      s <.
      dip (dup @| LocalStorage.get_heap) <:
      debug_msg "pre insert ++" <:
      LocalStorage.Heap.insert <:
      debug_msg "post insert ++" <:
      LocalStorage.set_heap
    )

  let clear_bids (type r) : (t * (Proto_cards.t * r), t * r) code =
    let clear_bid (type r) : (Bid.t * (t * (Proto_cards.t * r)), bool * (t * r)) code =
      let compute_max (type a) : (Bid.t * (Proto_card.Rarity.t * a), quantity * a) code =
        fun s -> !: (
          s <.
          Bid.get_price <:
          div
        ) in
      let do_nothing (type r) : (_, t * r) code = fun s -> !: (
          s <.
          drop <: drop <: drop
        ) in
      let remove_top_element (type r) : (t * r, t * r) code = fun s -> !: (
          s <.
          dup <:
          LocalStorage.get_heap <: LocalStorage.Heap.pop <: drop <:
          LocalStorage.set_heap
        ) in
      let get_diff : (_, _) code = fun s -> !: (
          s <. swap <: sub_natnat
        ) in
      let do_thing (type r) : (nat * (nat * (Bid.t * (t * r))), t * r) code =
        fun s -> !: (
          s <.
          diiip remove_top_element <:
          get_diff <: force_nat <:
          dip (dup @| Bid.get_quantity) <:
          swap <: sub_natnat <:
          dup <: gt <:
          if_bool ~target:(LocalStorage.ty @: tail @@ tail @@ tail @@ tail @@ s)
            (fun s -> !: (s <. force_nat <: Bid.change_quantity <: insert_bid))
            (fun s -> !: (s <. drop <: drop))
        ) in
      fun s -> !: (
          s <.
          dip (keep1 (fun s -> !: (
              s <.
              LocalStorage.get_id <:
              Proto_cards.get_rarity
            ))) <:
          dip swap <:
          keep1 compute_max <:
          diip (save_1_1 LocalStorage.get_total_quantity) <:
          bubble_down_2 <:
          keep2 cmp_ge_nat <: bubble_2 <: (* Is Max Quantity superior Total Quantity *)
          if_bool
            (do_nothing @| push_bool false)
            (do_thing @| push_bool true)
        ) in
    fun s -> !: (
        s <.
        dup <: LocalStorage.get_heap <: LocalStorage.Heap.size <: int <: gt <:
        loop (fun s -> !: (
            s <.
            dup <: LocalStorage.get_heap <: LocalStorage.Heap.get <:
            debug_msg "pre clear" <:
            diip dup <: clear_bid <:
            debug_msg "post clear" <:
            swap <: dup <: LocalStorage.get_heap <: LocalStorage.Heap.size <: int <: gt <:
            dip swap <: bool_and <:
            dip (dup @| LocalStorage.get_heap @| debug_msg "end cleanup loop" @| drop)
          )) <:
        dip drop
      )

  let add_bid (type r) : (quantity * (t * (Proto_cards.t * r)), t * r) code =
    let check_timestamp (type r) : (t * r, t * r) code = fun s -> !: (
        s <.
        now <: dip (dup @| car) <:
        dip (dup @| car) <: keep1 assert_cmp_ge_timestamp <:
        dip cdr <: swap <: assert_cmp_ge_timestamp
      ) in
    let get_price (type r) : (quantity * r, nat * r) code = fun s -> !: (
        s <.
        assert_positive_nat <:
        amount <: tez_nat <: force_ediv <:
        unpair <: swap <:
        gt_nat <: if_bool (add_n 1) noop
      ) in
    fun s -> !: (
        s <.
        dip check_timestamp <:
        keep1 get_price <:
        swap <: diip LocalStorage.increase_max_priority <:
        Bid.make <:
        debug_msg "pre insert" <:
        insert_bid <:
        debug_msg "post insert" <:
        clear_bids
      )

  let increase_bid (type r) : (quantity * (Bid.t * (t * (Proto_cards.t * r))), t * r) code =
    let get_new_quantity (type r) : (quantity * (Bid.t * r), quantity * r) code = fun s -> !: (
        s <.
        swap <:
        car <: car <:
        amount_nat <: div <:
        min_nat
      ) in
    let get_total_price (type r) : (Bid.t * r, nat * r) code = fun s -> !: (
        s <.
        car <: unpair <: mul_natnat
      ) in
    let make_new_partial_bid (type r) : (quantity * (Bid.t * r), Bid.Price.t * (Bid.Quantity.t * r)) code = fun s -> !: (
        s <.
        dip dup <: get_new_quantity <:
        swap <:
        get_total_price <: amount_nat <: add_natnat
      ) in
    fun s -> !: (
      s <.
      diip (LocalStorage.increase_max_priority) <:
      dip (dup @| dip swap) <:
      make_new_partial_bid <: Bid.make <:
      swap <: diip (save_1_1 LocalStorage.get_heap) <:
      LocalStorage.Heap.update_increase <:
      LocalStorage.set_heap <:
      clear_bids
    )

  let close_auction (type r) : (t * r, Bid.t list * r) code =
    fun s -> !: (
        s <.
        LocalStorage.get_heap <:
        cons_nil Bid.ty <: swap <:
        save_1_1 LocalStorage.Heap.size <: gt_nat <:
        loop (fun s -> !: (
            s <.
            save_1_1 LocalStorage.Heap.get <:
            dip swap <:
            cons_list <:
            swap <:
            save_1_1 LocalStorage.Heap.size <: gt_nat
          )) <:
        drop
      )

end
