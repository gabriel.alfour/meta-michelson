open Contract
open Proto_alpha
open Alpha_context
open Script_typed_ir
open Michelson_wrap

module type WRAP = sig
  type t
  val ty : t ty
end
let wrap (type t) (ty : t ty) : (module WRAP with type t = t) = (
  module struct
    type nonrec t = t
    let ty = ty
  end)

module Union (A:WRAP) (B:WRAP) : WRAP = struct
  type t = (A.t, B.t) union
  let ty = Types.union A.ty B.ty
end

module Pair (A:WRAP) (B:WRAP) : WRAP with type t = A.t * B.t  = struct
  type t = A.t * B.t
  let ty = Types.pair A.ty B.ty
end

module type COMPARABLE_WRAP = sig
  include WRAP
  val comparable_ty : t comparable_ty
end

module type T = sig type t end
let t_M (type t) : (module T) = (module struct type nonrec t = t end)

module Nat = struct
  type 'a nat_n = nat

  module type NAT = sig
    type t

    val ty : t ty
    val comparable_ty : t comparable_ty
    val to_nat : t -> nat
    val of_nat : nat -> t
    val push : int -> ('r, t * 'r) code
    val rename_to : (nat * 'r, t * 'r) code
    val rename_from : (t * 'r, nat * 'r) code
  end
  module Make (T:T) : NAT = struct
    type t = T.t nat_n

    let ty = Types.nat
    let comparable_ty = Types.nat_k
    let to_nat : t -> nat = fun x -> x
    let of_nat : nat -> t = fun x -> x

    let push n : ('r, t * 'r) code = push_nat n
    let rename_to : (nat * 'r, t * 'r) code = noop
    let rename_from : (t * 'r, nat * 'r) code = noop
  end
end

module Sized_map (A:COMPARABLE_WRAP) (B:WRAP) : sig
  type t
  val ty : t ty
  val empty : ('s, t * 's) code
  val append : ((A.t * (B.t * (t * 'rest))), (t * 'rest)) code
  val get : (A.t * (t * 'r), B.t * 'r) code
  val get_opt : (A.t * (t * 'r), B.t option * 'r) code
  val set : (A.t * (B.t * (t * 'r)), t * 'r) code
end = struct
  type t = (A.t, B.t) map * Script_int.n Script_int.num
  let ty = Types.pair (Map_t (A.comparable_ty, B.ty, None)) Types.nat

  let empty : ('s, t * 's) code = fun s ->
    descr s (stack ty s) (Const (Script_ir_translator.empty_map A.comparable_ty, Values.nat 0))

  let append : ((A.t * (B.t * (t * 'rest))), (t * 'rest)) code = fun bef ->
    ((dip (some @| dip (unpair @| dip (push_nat 1 @| add_natnat))) @| map_update) @| pair) bef

  let get_opt : (A.t * (t * 'r), B.t option * 'r) code = fun bef ->
    !: (bef <. dip car <: (map_get A.ty B.ty))

  let get : (A.t * (t * 'r), B.t * 'r) code = fun bef ->
    !: (bef <. get_opt <: force_some)
  
  let set : (A.t * (B.t * (t * 'r)), t * 'r) code = fun bef ->
    !: (bef <. diip unpair <: dip some <: map_update <: pair)
end

module Big_map (K:WRAP) (V:WRAP) = struct
  type t = (bytes, V.t) big_map
  let ty = Types.big_map Types.bytes_k V.ty

  let get : (K.t * (t * 'r), V.t * 'r) code = fun s ->
    !: (s <. pack K.ty <: blake2b <: big_map_get Types.bytes V.ty <: force_some)

  let set : (K.t * (V.t * (t * 'r)), t * 'r) code = fun s ->
    !: (s <. pack K.ty <: blake2b <: dip some <: big_map_set)
end


type bbm = (bytes, bytes) big_map

module KV_Big_map (K : WRAP) (V : WRAP) = struct
  type t = bbm

  let get_opt : (K.t * (t * 'r), V.t option * 'r) code = fun s ->
    !: (s <. pack K.ty <: blake2b <: big_map_get Types.bytes Types.bytes <: if_none (push_none V.ty) (unpack V.ty @| some))
  
  let get : (K.t * (t * 'r), V.t * 'r) code = fun s ->
    !: (s <. pack K.ty <: blake2b <: big_map_get Types.bytes Types.bytes <: force_some ~msg:"Big_map get" <: unpack V.ty)

  let set = fun s ->
    !: (s <. pack K.ty <: blake2b <: dip (pack V.ty @| some) <: big_map_set)

  let del = fun s ->
    !: (s <. pack K.ty <: blake2b <: dip (push_none Types.bytes) <: big_map_set)

  let wrap_k x =
    let open Alpha_wrap in
    blake2b @@ pack K.ty x

  let wrap_v x =
    let open Alpha_wrap in
    pack V.ty x

  let unwrap_v x =
    let open Alpha_wrap in
    unpack V.ty x
  
  let raw_get_bytes : t -> K.t -> MBytes.t = fun bm key ->
    match Script_ir_translator.map_get (wrap_k key) bm.diff with
    | Some (Some v) -> v
    | _ -> raise @@ Failure "raw_get_bytes : none"
    
  let raw_get : t -> K.t -> V.t = fun bm key ->
    unwrap_v @@ raw_get_bytes bm key

  let raw_mem : t -> K.t -> bool = fun bm key ->
    Script_ir_translator.map_mem (wrap_k key) bm.diff

  let raw_set : t -> K.t -> V.t -> t = fun bm key value ->
    {
      bm with diff = Script_ir_translator.map_update (wrap_k key) (Some (Some(wrap_v value))) bm.diff
    }

  let assert_kv ~(msg:string) : t -> (K.t * V.t) -> unit = fun bm (key, expected_value) ->
    let value_bytes = raw_get_bytes bm key in
    let value = unwrap_v value_bytes in
    if (value_bytes <> wrap_v expected_value) then (
      let actual_value_str = Cast.data_to_string V.ty value in
      let expected_value_str = Cast.data_to_string V.ty expected_value in
      Format.printf "Expected value : %s\n" expected_value_str ;
      Format.printf "Actual value : %s\n" actual_value_str ;
      raise (Failure (Format.sprintf "Assert_kv fail : %s\n" msg))
    ) ;
    ()
    
  let assert_k ~(msg:string) : t -> K.t -> unit = fun bm key ->
    let mem = raw_mem bm key in
    if (not mem) then (
      Format.printf "Assert_k fail : %s\n" msg ;
    ) ;
    ()
  
end

module KV_Big_bimap (K : WRAP) (V : WRAP) = struct
  type t = bbm
  let ty = Types.(big_map bytes_k bytes)
  
  module Ltr = KV_Big_map(K)(V)
  module Rtl = KV_Big_map(V)(K)

  let get_k (type r) : (K.t * (t * r), V.t * r) code = fun s -> !: (s <. Ltr.get)
  let get_k_opt (type r) : (K.t * (t * r), V.t option * r) code = fun s -> !: (s <. Ltr.get_opt)
  let get_v (type r) : (V.t * (t * r), K.t * r) code = fun s -> !: (s <. Rtl.get)

  let del_k (type r) : (K.t * (t * r), t * r) code = fun s -> !: (s <. keep2 get_k <: Ltr.del <: swap <: Rtl.del)
  let del_v (type r) : (V.t * (t * r), t * r) code = fun s -> !: (s <. keep2 get_v <: Rtl.del <: swap <: Ltr.del)
  let del_kv (type r) : (K.t * (V.t * (t * r)), t * r) code = fun s -> !: (s <. dip Rtl.del <: Ltr.del)

  let set_k (type r) : (K.t * (V.t * (t * r)), t * r) code = fun s ->
    !: (s <. keep2 Ltr.set <: swap <: Rtl.set)
  let set_v (type r) : (V.t * (K.t * (t * r)), t * r) code = fun s ->
    !: (s <. keep2 Rtl.set <: swap <: Ltr.set)

  let empty : t = Values.empty_big_map Types.bytes Types.bytes_k Types.bytes
  let raw_get_k = Ltr.raw_get
  let raw_get_v = Rtl.raw_get
  let raw_mem_k = Ltr.raw_mem
  let raw_mem_v = Rtl.raw_mem
  let raw_set_k bm k v =
    let bm = Ltr.raw_set bm k v in
    Rtl.raw_set bm v k

  let assert_kv = Ltr.assert_kv
  let assert_k = Ltr.assert_k
end

module Hash_map (A:WRAP) (B:WRAP) : sig

end = struct
  type t = (bytes, B.t) map
  let ty = Map_t (Types.bytes_k, B.ty, None)

  let empty : ('s, t * 's) code = fun s ->
    descr s (stack ty s) (Const (Script_ir_translator.empty_map Types.bytes_k))

  let pack = fun s -> pack A.ty s

  let get : (A.t * (t * 'r), B.t * 'r) code = fun s ->
    !: (s <. pack <: map_get Types.bytes B.ty <: force_some)

  let set : (A.t * (B.t * (t * 'r)), t * 'r) code = fun s ->
    !: (s <. pack <: dip some <: map_update)
end

module Priority_heap(K:Nat.NAT)(P:COMPARABLE_WRAP)(V:WRAP) = struct
  module Value = (val wrap (Types.pair P.ty V.ty))

  module SM = Sized_map(K)(Value)

  type key = K.t
  type value = Value.t
  type kv = key * value
  let kv_ty = Types.pair K.ty Value.ty

  let swap_keys : (
    kv * (kv * (SM.t * 'r)),
    kv * (kv * (SM.t * 'r))
  ) code =
    fun bef -> !: (bef <. keep2 (cdr @| dip cdr) <: car <: dip car <: dip (pair @| swap) <: pair)
  let swap_places : (((K.t * Value.t)) * ((K.t * Value.t) * (SM.t * 'r)), SM.t * 'r) code = fun bef ->
    !: (bef <. swap_keys <: dip (unpair @| SM.set) <: unpair <: SM.set)
  
  
  (*
    Percolate up. Given an element key and priority, move it up until it's not needed.
  *)
  let percolate_up (type r) : ((K.t * P.t) * (SM.t * r), _ (* SM.t * r *)) code = fun bef ->
    let get_child_index_opt : (K.t * (SM.t * 'r), K.t option * (SM.t * 'r)) code = fun bef ->
      !: (bef <.
          K.rename_from <: push_nat 2 <: div <:
          keep1 (nat_neq) <: swap <: if_bool (K.rename_to @| some) (drop @| push_none K.ty)) in

    let _percolate_up_step : ((K.t * Value.t) * (SM.t * 'r), (K.t * Value.t) option * (SM.t * 'r)) code = fun bef ->
      let none_branch : (_, _ * (SM.t * 'r)) code = drop @| push_none (Types.pair K.ty Value.ty) in
      let some_branch : (K.t * ((K.t * Value.t) * (SM.t * 'r)), (K.t * Value.t) option * (SM.t * 'r)) code = fun bef ->
        let get_child : (
          K.t * ((K.t * Value.t) * (SM.t * 'r)),
          ((K.t * Value.t)) * ((K.t * Value.t) * (SM.t * 'r))) code = fun bef ->
          !: (bef <. swap <: dip (dip dup @| keep1 SM.get @| pair) <: swap) in
        let child_greater : _ code = fun bef ->
          !: (bef <. cdar <: dip cdar <: cmp P.comparable_ty <: gt) in
        let true_branch : _ code = fun bef -> !: (bef <. keep1 swap_places) in
        let false_branch = fun bef -> !: (bef <. dip drop) in
        !: (bef <. get_child <: keep2 child_greater <: bubble_2 <: if_bool true_branch false_branch <: some)
      in
      !: (bef <.
          keep1 (fun s-> !:(s<.car<:get_child_index_opt)) <: swap @| if_none none_branch some_branch ) in
    noop bef

  let keep_get_opt_kv (type s) : ((key * (SM.t * s)), kv option * (SM.t * s)) code = fun bef ->
    let none_branch : (key * (SM.t * s), _) code = fun bef -> !: (bef <. drop <: push_none kv_ty) in
    let some_branch : (value * (key * (SM.t * s)), _) code = fun bef -> !: (bef <. swap <: pair <: some) in
    !: (bef <. keep2 SM.get_opt <: bubble_2 <: if_none none_branch some_branch)

  let keep_get_kv : (key * (SM.t * 'r), kv * (SM.t * 'r)) code = fun bef ->
      !: (bef <. keep2 SM.get <: dip swap <: pair)
  
  let select_down (type r) : (kv * (SM.t * r), kv option * (SM.t * r)) code = fun bef ->
    let get_children (type r) : (key * (SM.t * r), kv option * (kv option * (SM.t * r))) code = fun bef ->
      let get_keys =
        bef <.
        K.rename_from <: push_nat 2 <: mul_natnat <: push_nat 1 <: swap <:
        keep1 add_natnat <:K.rename_to <: dip K.rename_to in
      let get_kvs =
        get_keys <: dip swap <: keep_get_opt_kv <: dip (swap @| keep_get_opt_kv) in
      !: get_kvs in
    let children_to_list (type r) : (kv option * (kv option * (SM.t * r)), kv list * (SM.t * r)) code = fun bef ->
      !: (bef <. cons_nil kv_ty <: swap <: if_none noop cons_list <: swap <: if_none noop cons_list) in
    let aux (type r) : (kv * (kv * (bool * r)), kv * (bool * r)) code = fun bef ->
      !: (bef <. swap <: keep2 (cdar @| dip cdar @| cmp P.comparable_ty @| gt) <: bubble_2 <:
          if_bool drop (dip drop @| dip (drop @| push_bool true))
         ) in
    let opt_of_pb (type r) : (kv * (bool * r), kv option * r) code = fun bef ->
      !: (bef <. swap <: if_bool some (drop @| push_none kv_ty)) in
    !: (
      bef <. keep1 (car @| get_children @| children_to_list) <: swap <: diip (push_bool false) <: list_iter aux <:
      opt_of_pb
    )

  let percolate_down_step (type r) : (kv * (SM.t * r), (kv, unit) union * (SM.t * r)) code = fun bef ->
    !: (bef <. keep1 select_down <: swap <: if_none (drop @| push_unit @| right kv_ty) (keep1 swap_places @| left Types.unit))

  let percolate_down (type r) : (key * (SM.t * r), SM.t * r) code = fun bef ->
    let percolate_loop = loop_left percolate_down_step in
    !: (bef <. keep_get_kv <: left Types.unit <: percolate_loop <: drop)
    
end
