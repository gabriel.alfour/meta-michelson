module Test_helpers = Tezos_alpha_test_helpers
open Test_helpers.Proto_alpha
module AC = Alpha_context
open Script_typed_ir
open Contract

let descr bef aft instr =
  {
    loc = 0 ;
    bef ; aft ; instr
  }

type 'a st = 'a stack_ty
type ('a, 'b) code = ('a st) -> ('a, 'b) descr

let unstack (item: (('a * 'rest) stack_ty)) : ('a ty * 'rest stack_ty) =
  let Item_t (hd, tl, _) = item in
  (hd, tl)

let nil = Empty_t
let head x = fst @@ unstack x
let tail x = snd @@ unstack x

let stack a b = Item_t (a, b, None)

let some : ('a * 'rest, 'a option * 'rest) code = fun bef ->
  let (hd, tl) = unstack bef in
  descr bef (stack (Contract.Types.option hd) tl) Cons_some

let dup : ('a * 'rest, 'a * ('a * 'rest)) code = fun bef ->
  let Item_t (ty, rest, _) = bef in
  descr bef (Item_t (ty, Item_t (ty, rest, None), None)) Dup

let drop : ('a * 'rest, 'rest) code = fun bef ->
  let aft = snd @@ unstack bef in
  descr bef aft Drop

let swap (bef : (('a * ('b * 'c)) stack_ty)) =
  let Item_t (a, Item_t (b, rest, _), _) = bef in
  descr bef (Item_t (b, (Item_t (a, rest, None)), None)) Swap

let car (bef : (('a, 'b) pair * 'rest) stack_ty) =
  let Item_t (pair, rest, _) = bef in
  let a = match pair with
    | Pair_t ((a, _, _), _, _) -> a
    | _ -> assert false in
  descr bef (Item_t (a, rest, None)) Car

let cdr (bef : (('a, 'b) pair * 'rest) stack_ty) =
  let Item_t (pair, rest, _) = bef in
  let b = match pair with
    | Pair_t (_, (b, _, _), _) -> b
    | _ -> assert false in
  descr bef (Item_t (b, rest, None)) Cdr

let dip code (bef : ('ty * 'rest) stack_ty) =
  let Item_t (ty, rest, _) = bef in
  let applied = code rest in
  let aft = Item_t (ty, applied.aft, None) in
  descr bef aft (Dip (code rest))

let diip c x = dip (dip c) x
let diiip c x = dip (diip c) x
let diiiip c x = dip (diiip c) x

let loop (code : ('s, bool * 's) code) : ((bool * 's), 's) code = fun bef ->
  let aft = tail bef in
  descr bef aft @@ Loop (code aft)

let seq a b bef =  
  let a_descr = a bef in
  let b_descr = b a_descr.aft in
  let aft = b_descr.aft in
  descr bef aft @@ Seq (a_descr, b_descr)

let (@>) (stack : 'b st) (code : ('a, 'b) code) = code stack
let (@|) = seq
let (@:) = stack

type ('a, 'b) interpreter = ('a, 'b) descr

let (<.) (stack:'a st) (code: ('a, 'b) code): ('a, 'b) interpreter =
  let descr = code stack in
  descr

let (<:) (ab_descr:('a, 'b) interpreter) (code:('b, 'c) code) : ('a, 'c) interpreter =
  let bc_descr = code ab_descr.aft in
  descr ab_descr.bef bc_descr.aft @@ Seq (ab_descr, bc_descr)

let to_descr (descr : ('a, 'b) interpreter) = descr

let (!:) = to_descr

type nat = AC.Script_int.n AC.Script_int.num
type int_num = AC.Script_int.z AC.Script_int.num
type bytes = MBytes.t
type address = Proto_alpha.Alpha_context.Contract.t ty
type mutez = Proto_alpha.Alpha_context.Tez.t ty

let bubble_1 = swap
let bubble_down_1 = swap

let bubble_2 : ('a * ('b * ('c * 'r)), 'c * ('a * ('b * 'r))) code = fun bef ->
  !: (bef <. dip swap <: swap)
let bubble_down_2 : ('a * ('b * ('c * 'r)), ('b * ('c * ('a * 'r)))) code = fun bef ->
  !: (bef <. swap <: dip swap)

let bubble_3 : ('a * ('b * ('c * ('d * 'r))), 'd * ('a * ('b * ('c * 'r)))) code = fun bef ->
  !: (bef <. diip swap <: dip swap <: swap)

let cmp c_ty : _ code = fun bef ->
  let aft = stack Types.int @@ tail @@ tail @@ bef in
  descr bef aft (Compare c_ty)

let cmp_bytes : (bytes * (bytes * 'r), (int_num * 'r)) code = fun bef ->
  let aft = stack Types.int @@ snd @@ unstack @@ snd @@ unstack bef in
  descr bef aft @@ Compare (Bytes_key None)

let eq : (int_num * 'r, bool *'r) code = fun bef ->
  let aft = stack Types.bool @@ snd @@ unstack bef in
  descr bef aft Eq

let neq : (int_num * 'r, bool *'r) code = fun bef ->
  let aft = stack Types.bool @@ snd @@ unstack bef in
  descr bef aft Neq

let abs : (int_num * 'r, nat *'r) code = fun bef ->
  let aft = stack Types.nat @@ snd @@ unstack bef in
  descr bef aft Abs_int

let int : (nat * 'r, int_num*'r) code = fun bef ->
  let aft = stack Types.int @@ snd @@ unstack bef in
  descr bef aft Int_nat

let nat_opt : (int_num * 'r, nat option * 'r) code = fun bef ->
  let aft = stack Types.(option nat) @@ tail bef in
  descr bef aft Is_nat

let bool_and (type r) : (bool * (bool * r), bool * r) code = fun bef ->
  let aft = Types.bool @: tail @@ tail bef in
  descr bef aft And

let bool_or (type r) : (bool * (bool * r), bool * r) code = fun bef ->
  let aft = Types.bool @: tail @@ tail bef in
  descr bef aft Or

let nat_neq = fun s -> (int @| neq) s

let noop : ('r, 'r) code = fun bef ->
  descr bef bef Nop

let fail aft : ('a * 'r, 'b) code = fun bef ->
  let head = fst @@ unstack bef in
  descr bef aft (Failwith head)

let push_string str (bef : 'rest stack_ty) : (_, (string * 'rest)) descr =
  let aft = Item_t (Types.string, bef, None) in
  descr bef aft (Const (str))

let push_none (a:'a ty) : ('rest, 'a option * 'rest) code = fun r ->
  let aft = stack (Types.option a) r in
  descr r aft (Const None)

let push_unit : ('rest, unit * 'rest) code = fun r ->
  let aft = stack Types.unit r in
  descr r aft (Const ())

let failstring str aft =
  push_string str @| fail aft

let ucar (bef : (('a, 'b) pair * 'rest) stack_ty) =
  (seq dup car) bef

let carcdr (bef : (('a, 'b) pair * (('a, 'b) pair * 'rest)) stack_ty) =
  (car @| dip cdr) bef

let cdrcar = fun s -> !: (s <. cdr <: dip car)
    
let cdrcdr = fun s -> !: (s <. cdr <: dip cdr)
    
let carcar = fun s -> !: (s <. car <: dip car)

let cdar = fun s -> (cdr @| car) s

let unpair (bef : (('a, 'b) pair * 'rest) stack_ty) =
  (dup @| car @| dip cdr) bef

let add_natnat (bef : (nat * (nat * 'rest)) stack_ty) =
  let Item_t (nat, Item_t (_, rest, _), _) = bef in
  let aft = Item_t (nat, rest, None) in
  descr bef aft Add_natnat

let add_teztez : (AC.Tez.tez * (AC.Tez.tez * 'rest), _) code = fun bef ->
  let aft = tail bef in
  descr bef aft Add_tez

let mul_natnat (bef : (nat * (nat * 'rest)) stack_ty) =
  let nat = head bef in
  let rest = tail @@ tail bef in
  let aft = stack nat rest in
  descr bef aft Mul_natnat

let sub_int : (int_num * (int_num * 'r), int_num * 'r) code = fun bef ->
  let aft = tail bef in
  descr bef aft Sub_int

let sub_natnat : (nat * (nat * 'r), int_num * 'r) code =
  fun bef -> !: (bef <. int <: dip int <: sub_int)

let amount : ('r, AC.Tez.t * 'r) code = fun bef ->
  let aft = Types.mutez @: bef in
  descr bef aft Amount

let pack (ty:'a ty) : ('a * 'r, bytes * 'r) code = fun bef ->
  let aft = stack Types.bytes @@ tail bef in
  descr bef aft (Pack ty)

let unpack_opt : type a . a ty -> (bytes * 'r, a option * 'r) code = fun ty bef ->
  let aft = stack (Types.option ty) (tail bef) in
  descr bef aft (Unpack ty)

let push_nat n (bef : 'rest stack_ty) : (_, (nat * 'rest)) descr =
  let aft = Item_t (Types.nat, bef, None) in
  descr bef aft (Const (Contract.Values.nat n))

let push_tez n (bef : 'rest stack_ty) : (_, (AC.Tez.tez * 'rest)) descr =
  let aft = Types.mutez @: bef in
  descr bef aft (Const (Contract.Values.tez n))

let push_bool b : ('s, bool * 's) code = fun bef ->
  let aft = stack Types.bool bef in
  descr bef aft (Const b)

let map_update (bef : (('a * ('b option * (('a, 'b) map * ('rest)))) stack_ty)) : (_, ('a, 'b) map * 'rest) descr =
  let Item_t (_, Item_t (_, Item_t (map, rest, _), _), _) = bef in
  let aft = Item_t (map, rest, None) in
  descr bef aft Map_update

let map_get : 'a ty -> 'b ty -> ('a * (('a, 'b) map * 'r), 'b option * 'r) code = fun _a b bef ->
  let base = snd @@ unstack @@ snd @@ unstack bef in
  let aft = stack (Types.option b) base in
  descr bef aft Map_get

let big_map_get : 'a ty -> 'b ty -> ('a * (('a, 'b) big_map * 'r), 'b option * 'r) code = fun _a b bef ->
  let base = snd @@ unstack @@ snd @@ unstack bef in
  let aft = stack (Types.option b) base in
  descr bef aft Big_map_get

let big_map_set : ('a * ('b option * (('a, 'b) big_map * 'r)), ('a, 'b) big_map * 'r) code = fun bef ->
  let base = tail @@ tail bef in
  descr bef base Big_map_update

let cons_nil : type ele a . ele ty -> (a, ele list * a) code = fun ele bef ->
  let aft = stack (Types.list ele) bef in
  let result : (a, ele list * a) descr = descr bef aft Nil in
  result

let cons_list : type a s . (a * (a list * s), a list * s) code = fun bef ->
  let aft = tail bef in
  descr bef aft Cons_list

let if_cons : type elem b c . target:(c st) -> (elem * (elem list * b), c) code -> (b, c) code -> (elem list * b, c) code =
  fun ~target cons_branch nil_branch bef ->
    let (lst, aft) = unstack bef in
    let a = (function | List_t (a, _) -> a | _ -> raise @@ Failure "if_cons") lst in
    let cons_descr = cons_branch @@ stack a @@ stack (Types.list a) aft in
    let nil_descr = nil_branch aft in
    descr bef target (If_cons (cons_descr, nil_descr))

let if_bool ?target true_branch false_branch : (bool * 'r, 's) code = fun bef ->
  let base = tail bef in
  let aft = Option.unopt ~default:((true_branch base).aft) target in
  descr bef aft (If (true_branch base, false_branch base))

let if_none ?target none_branch some_branch : ('a option * 'r, 'b) code = fun bef ->
  let (a_opt, base) = unstack bef in
  let a = (function | Option_t ((a, _), _, _) -> a | _ -> raise @@ Failure "if_none") a_opt in
  let target = Option.unopt ~default:(none_branch base).aft target in
  descr bef target (If_none (none_branch base, some_branch (stack a base)))

let force_some ?msg : ('a option * 'r, 'a * 'r) code = fun s ->
  let (a_opt, base) = unstack s in
  let a = (function | Option_t ((a, _), _, _) -> a | _ -> raise @@ Failure "if_none") a_opt in
  let target = a @: base in
  if_none ~target (failstring ("force_some : " ^ Option.unopt ~default:"" msg) target) noop s

let unpack ty = fun bef ->
  !: (bef <. unpack_opt ty <: force_some)

let tez_nat = fun s ->
  !: (s <. pack Types.mutez <: unpack Types.nat)

let amount_nat : ('r, nat * 'r) code = fun bef ->
  !: (bef <. amount <: pack Types.mutez <: unpack Types.nat)

let loop_left ?after (code: ('a * 'r, ('a, 'b) union * 'r) code): (('a, 'b) union * 'r, 'b * 'r) code = fun bef ->
  let (union, base) = unstack bef in
  let a = (function | Union_t ((a, _), (_, _), _) -> a | _ -> raise @@ Failure "loop_left") union in
  let b = (function | Union_t ((_, _), (b, _), _) -> b | _ -> raise @@ Failure "loop_left") union in
  let code_stack = a @: base in
  let aft = Option.unopt ~default:(b @: base) after in
  descr bef aft (Loop_left (code code_stack))

let bytes_concat : (MBytes.t * (MBytes.t * 'rest), MBytes.t * 'rest) code = fun bef ->
  let (_, aft) = unstack bef in
  descr bef aft Concat_bytes_pair

let sha256 : (MBytes.t * 'rest, MBytes.t * 'rest) code = fun bef ->
  descr bef bef Sha256

let blake2b : (MBytes.t * 'rest, MBytes.t * 'rest) code = fun bef ->
  descr bef bef Blake2b

let pair (bef : ('a * ('b * 'rest)) stack_ty) : (_, ('a * 'b) * 'rest) descr =
  let Item_t (a, Item_t (b, rest, _), _) = bef in
  let aft = Item_t (Pair_t ((a, None, None), (b, None, None), None), rest, None) in
  descr bef aft Cons_pair

let ediv : (nat * (nat * 'r), (nat * nat) option * 'r) code = fun s ->
  let (n, base) = unstack @@ snd @@ unstack s in
  let aft = Types.option (Types.pair n n) @: base in
  descr s aft Ediv_natnat

let ediv_tez = fun s ->
  let aft = Types.(option @@ pair (head s) (head s)) @: tail @@ tail s in
  descr s aft Ediv_teznat

let force_ediv x = (ediv @| force_some) x
let force_ediv_tez x = (ediv_tez @| force_some) x

let div x = (force_ediv @| car) x

let div_n n = fun s -> !: (s <. push_nat n <: swap <: div)
let add_n n = fun s -> !: (s <. push_nat n <: swap <: add_natnat)
let add_teztez_n n = fun s -> !: (s <. push_tez n <: swap <: add_teztez)
let sub_n n = fun s -> !: (s <. push_nat n <: swap <: sub_natnat)
let eq_n n = fun s -> !: (s <. sub_n n <: eq)

let keep1 : type r s . ('a * r, s) code -> ('a * r, 'a * s) code = fun code bef ->
  (dup @| dip code) bef

let save_1_1 : type r . ('a * r, 'b * r) code -> ('a * r, 'b * ('a * r)) code = fun code s ->
  !: (s <. dup <: dip code <: swap)

let keep2 : type r s . ('a * ('b * r), s) code -> ('a * ('b * r), ('a * ('b * s))) code = fun code bef ->
  (dup @| dip (swap @| dup @| dip (swap @| code))) bef

let keep_1_2 : type r s . ('a * ('b * r), s) code -> ('a * ('b * r), 'b * s) code = fun code bef ->
  (dip dup @| swap @| dip code) bef

let list_iter : type a r . (a * r, r) code -> (a list * r, r) code = fun code bef ->
  let aft = tail bef in
  let a_lst = head bef in
  let a = match a_lst with
    | List_t (a, _) -> a
    | _ -> raise @@ Failure "list_iter" in
  descr bef aft (List_iter (code (stack a aft)))

let left (b:'b ty) : ('a * 'r, ('a, 'b) union * 'r) code = fun bef ->
  let (a, base) = unstack bef in
  let aft = Types.union a b @: base in
  descr bef aft Left

let right (a:'a ty) : ('b * 'r, ('a, 'b) union * 'r) code = fun bef ->
  let (b, base) = unstack bef in
  let aft = Types.union a b @: base in
  descr bef aft Right

let ge : (int_num * 'r, bool * 'r) code = fun bef ->
  let base = tail bef in
  let aft = stack Types.bool base in
  descr bef aft Ge

let gt : (int_num * 'r, bool * 'r) code = fun bef ->
  let base = tail bef in
  let aft = stack Types.bool base in
  descr bef aft Gt

let lt : (int_num * 'r, bool * 'r) code = fun bef ->
  let base = tail bef in
  let aft = stack Types.bool base in
  descr bef aft Lt

let gt_nat = fun s -> !: (s <. int <: gt)

let assert_positive_nat = fun s -> !: (s <. dup <: gt_nat <: if_bool noop (failstring "positive" s))

let cmp_ge_nat : (nat * (nat * 'r), bool * 'r) code = fun bef ->
  !: (bef <. sub_natnat <: ge)

let min_nat : (nat * (nat * 'r), nat * 'r) code = fun s -> !: (
    s <.
    keep2 cmp_ge_nat <: bubble_2 <:
    if_bool drop (dip drop)
  )

let cmp_ge_timestamp : (AC.Script_timestamp.t * (AC.Script_timestamp.t * 'r), bool * 'r) code = fun bef ->
  !: (bef <. cmp Types.timestamp_k <: ge)

let assert_cmp_ge_nat : (nat * (nat * 'r), 'r) code = fun bef ->
  !: (bef <. cmp_ge_nat <: if_bool noop (failstring "assert cmp ge nat" (tail @@ tail bef)))

let assert_cmp_ge_timestamp : (AC.Script_timestamp.t * (AC.Script_timestamp.t * 'r), 'r) code = fun bef ->
  !: (bef <. cmp_ge_timestamp <: if_bool noop (failstring "assert cmp ge timestamp" (tail @@ tail bef)))

let debug ~msg () : ('a, 'a) code = fun s -> !: (s <. push_string msg <: push_string "_debug" <: noop <: drop <: drop)

let debug_msg msg = debug ~msg ()

let force_nat = fun s -> !: (s <. nat_opt <: force_some ~msg:"force nat")

let now : ('r, AC.Script_timestamp.t * 'r) code = fun bef ->
  let aft = stack Types.timestamp bef in
  descr bef aft Now
