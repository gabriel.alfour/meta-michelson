open Base
open Meta_michelson_helpers.Michelson_wrap
open Meta_michelson_helpers.Contract

module Value = struct
  type t = string
  let ty = Types.string
  let cmp = fun s -> !: (s <. cmp Types.string_k)
  let compare = compare
end

module Key = struct
  type t = nat
  let ty = Types.nat
  let raw_to_int n =
    Option.unopt_exn (Failure "Linked Heap : raw_size")
      (AC.Script_int.to_int n)
  let raw_of_int n = Values.nat n
  let of_nat = noop
  let to_nat = noop
end
module LH = Meta_michelson_helpers.Heap.LinkedHeap(Key)(Value)

let empty_stack_ty : (LH.t * unit) st = stack LH.ty nil
let empty_stack : (LH.t * unit) Proto_alpha.Script_interpreter.stack = Item (LH.empty, Empty)

let toto () =
  let stack_ty = Types.(nat @: nil) in
  let stack = Proto_alpha.Script_interpreter.(Item (Values.nat 1 , Empty)) in
  let code = fun s -> !: (s <. push_nat 1 <: debug_msg "toto" <: drop) in
  Run.step_lwt stack (code stack_ty) >>=? fun _ ->
  return ()

let tata () =
  let stack_ty = Types.(nat @: nil) in
  let stack = Proto_alpha.Script_interpreter.(Item (Values.nat 1 , Empty)) in
  let code = fun s -> !: (s <. push_nat 1 <: noop <: drop) in
  Run.step_lwt stack (code stack_ty) >>=? fun _ ->
  return ()

let display ~code prev_stack new_stack descr =
  let code_repr = Meta_michelson_helpers.Cast.descr_to_string descr in
  let pre_repr = Meta_michelson_helpers.Cast.stack_to_string descr.bef prev_stack in
  let post_repr = Meta_michelson_helpers.Cast.stack_to_string descr.aft new_stack in
  if code then Format.printf "Code : %s\n" code_repr ;
  Format.printf "Pre Stack : %s\n" pre_repr ;
  Format.printf "New Stack : %s\n" post_repr ;
  ()

let insert () =
  let code = fun s -> !: (s <. push_string "to insert" <: LH.insert) in
  let code = code empty_stack_ty in
  Run.step_value LH.empty code >>=? fun bm ->
  LH.assert_iv ~msg:__LOC__ bm (Values.nat 1, "to insert") ;
  return ()

let debug_visitor (Ex_typed_stack (stack_ty, stack)) =
  let open Proto_alpha.Script_typed_ir in
  let open Proto_alpha.Script_interpreter in
  match stack_ty with
  | Item_t (String_t _,
            Item_t (Pair_t ((Big_map_t(Bytes_key _, Bytes_t _, None),_,_), (Nat_t(_),_,_),_), _, _), _) -> (
      let (Item(s, Item((bm, t), _))) = stack in
      let bm = LH.raw_make (bm, t) in
      Format.printf "Debug (%s) : %s\n" s @@ LH.raw_to_string (fun x -> x) bm
    )
  | _ -> Format.printf "Debug : %s\n" @@ Meta_michelson_helpers.Cast.stack_to_string stack_ty stack

let debug_visitor_silent _ = ()

let config = Run.make_config ~debug_visitor ()
let silent_config = Run.make_config ~debug_visitor:debug_visitor_silent ()

let multiple_inserts () =
  let lst = List.map (fun (a, b) -> (Values.nat a, b)) [
    (1, "42770") ;
    (2, "39190") ;
    (3, "26166") ;
  ] in
  let code = fun s -> !: (
      s <.
      push_string "10043" <: LH.insert
    ) in
  let descr = code empty_stack_ty in
  let lh = LH.raw_of_list lst in
  Run.step_value ~config lh descr >>=? fun bm ->
  LH.assert_iv ~msg:__LOC__ bm (Values.nat 1, "10043") ;
  return ()

let () =
  Random.self_init ()

let random_string () =
  let n = 10000 + Random.int 90000 in
  string_of_int n

let random_inserts () =
  let size = 50 in
  let ns = 1 -- size in
  let strs = List.map (fun _ -> random_string ()) ns in
  let sorted_strs = List.sort compare strs in
  let code = List.fold_left (fun code str -> code @| push_string str @| LH.insert) noop strs in
  let descr = code empty_stack_ty in
  Run.step_value LH.empty descr >>=? fun bm ->
  LH.assert_iv ~msg:__LOC__ bm (Values.nat 1, List.hd sorted_strs) ;
  LH.assert_size ~msg:__LOC__ bm size ;
  List.iter (fun n ->
      let msg = "random_inserts :" ^ (string_of_int n) in
      LH.assert_i ~msg bm @@ Values.nat n
    ) ns ;
  return ()

let pop_single () =
  let size = 1 in
  let ns = 1 -- size in
  let lst = List.map (fun i -> (Values.nat i, string_of_int i)) ns in
  let before_bm = LH.raw_of_list lst in
  let descr = LH.pop empty_stack_ty in
  Run.step_1_2 before_bm descr >>=? fun (value, after_bm) ->
  LH.assert_size ~msg:__LOC__ after_bm 0 ;
  assert (value = "1") ;
  return ()

let pop_set () =
  let size = 5 in
  let ns = 1 -- size in
  let lst = List.map (fun i -> (Values.nat i, string_of_int i)) ns in
  let before_bm = LH.raw_of_list lst in
  let descr = LH.pop empty_stack_ty in
  Run.step_1_2 ~config before_bm descr >>=? fun (value, after_bm) ->
  LH.assert_size ~msg:__LOC__ after_bm (size - 1) ;
  assert (value = "1") ;
  LH.assert_iv ~msg:__LOC__ after_bm (Values.nat 1, "2") ;
  return ()

let inserts_pops () =
  let size = 20 in
  let ns = 1 -- size in
  let strs = List.map (fun _ -> random_string ()) ns in
  let insert bm str =
    let descr = (push_string str @| LH.insert) empty_stack_ty in
    Run.step_value ~config:silent_config bm descr
  in
  fold_left_s insert LH.empty strs >>=? fun before_map ->
  let pop (bm, lst) _ =
    let descr = LH.pop empty_stack_ty in
    Run.step_1_2 ~config:silent_config bm descr >>=? fun (value, after_bm) ->
    return (after_bm, value :: lst)
  in
  fold_left_s pop (before_map, []) ns >>=? fun (_, lst) ->
  let lst = List.rev lst in
  assert (lst = List.sort compare strs) ;
  return ()

let upcrease () =
  let size = 50 in
  let ns = 1 -- size in
  let strs = List.map (fun _ -> random_string ()) ns in
  let new_strs = List.map (fun s -> "9" ^ s) strs in
  let changes = List.combine strs new_strs in
  let insert bm str =
    let descr = (push_string str @| LH.insert) empty_stack_ty in
    Run.step_value bm descr
  in
  fold_left_s insert LH.empty strs >>=? fun before_bm ->
  let descr = LH.update_increase (Value.ty  @: Value.ty @: empty_stack_ty) in
  fold_left_s (fun bm change ->
      Run.step_3_1 (fst change) (snd change) bm descr >>=? fun bm ->
      return bm
    ) before_bm changes >>=? fun after_bm ->
  LH.assert_heap_property ~msg:__LOC__ after_bm ;
  return ()
  
let main = "heap", [
    test "insert" insert ;
    test "inserts" multiple_inserts ;
    test "random inserts" random_inserts ;
    test "pop single" pop_single ;
    test "pop set" pop_set ;
    test "insert & pops" inserts_pops ;
    test "upcrease" upcrease ;
  ]
