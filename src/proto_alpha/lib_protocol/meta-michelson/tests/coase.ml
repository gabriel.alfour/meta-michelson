(*
 * Copyright (C) 2019, Coase, inc
*)

open Base
open Meta_michelson_helpers.Contract
open Meta_michelson_helpers.Michelson_wrap
open Meta_michelson_contracts.Coase
module Proto_alpha = Tezos_alpha_test_helpers.Proto_alpha
module AC = Proto_alpha.Alpha_context

let black_lotus = Proto_card.make @@ Values.nat 100
let green_mana = Proto_card.make @@ Values.nat 1
let proto_cards = Proto_cards.raw_of_list [black_lotus ; green_mana]

let timestamp_of_int n =
  AC.Script_timestamp.of_zint @@ Proto_alpha.Alpha_environment.Z.of_int n

let auction =
  let start = timestamp_of_int 10 in
  let over = timestamp_of_int 20 in
  Auction.LocalStorage.make start over (Values.nat 0)

let on_time_timestamp = timestamp_of_int 15

let debug_visitor (Ex_typed_stack (stack_ty, stack)) =
  let open Proto_alpha.Script_typed_ir in
  let open Proto_alpha.Script_interpreter in
  match stack_ty with
  | Item_t (String_t _,
            Item_t (Pair_t ((Big_map_t(Bytes_key _, Bytes_t _, None),_,_), (Nat_t(_),_,_),_), _, _), _) -> (
      let (Item(s, Item((bm, t), _))) = stack in
      let bm = Auction.LocalStorage.Heap.raw_make (bm, t) in
      Format.printf "Debug (%s) : %s\n" s @@ Auction.LocalStorage.Heap.raw_to_string Bid.raw_to_string bm
    )
  | _ -> Format.printf "Debug : %s\n" @@ Meta_michelson_helpers.Cast.stack_to_string stack_ty stack

let config = Run.make_config
    ~timestamp:on_time_timestamp
    ~amount:(Option.unopt_exn (Failure "Config amount") @@ AC.Tez.of_mutez @@ Int64.of_int 1000)
    ~debug_visitor
    ()

let pre_add_stack_ty = Types.nat @: Auction.ty @: Proto_cards.ty @: nil
let add_descr = Auction.add_bid pre_add_stack_ty
let add_single_bid () =
  Run.step_3_1 ~config (Values.nat 10) auction proto_cards add_descr >>=? fun r ->
  let heap = snd @@ snd @@ snd r in
  Format.printf "\n Bids :\n%s\n" @@ Auction.LocalStorage.Heap.raw_to_string Bid.raw_to_string heap;
  return ()

let add_early_bid () =
  let config = Run.make_config ~base_config:config ~timestamp:(timestamp_of_int 5) () in
  Run.step_3_1 ~config (Values.nat 10) auction proto_cards add_descr >>=? fun r ->
  let heap = snd @@ snd @@ snd r in
  Format.printf "\n Bids :\n%s\n" @@ Auction.LocalStorage.Heap.raw_to_string Bid.raw_to_string heap;
  return ()

let add_late_bid () =
  let config = Run.make_config ~base_config:config ~timestamp:(timestamp_of_int 25) () in
  Run.step_3_1 ~config (Values.nat 10) auction proto_cards add_descr >>=? fun r ->
  let heap = snd @@ snd @@ snd r in
  Format.printf "\n Bids :\n%s\n" @@ Auction.LocalStorage.Heap.raw_to_string Bid.raw_to_string heap;
  return ()

let main = "coase", [
    test "add bid" add_single_bid ;
    test "add early bid" add_early_bid ;
    test "add late bid" add_late_bid ;
  ]
  
